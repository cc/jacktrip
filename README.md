this is a public project on
> https://cm-gitlab.stanford.edu/public

copy a read-only snapshot with the download button

or clone it to a repository on a local machine 
> git clone git@cm-gitlab.stanford.edu:cc/jacktrip.git

(the repo's origin requires permission to be modified)

# JackTrip is a Linux, Mac OSX, or Windows multi-machine audio system used for network music performance over the Internet.
It supports any number of channels (as many as the computer/network can handle) of bidirectional, high quality, uncompressed audio signal streaming.

You can use it between any combination of machines e.g., one end using Linux can connect to another using Mac OSX.

# Raspberry Pi

[paper](https://lac.linuxaudio.org/2019/doc/chafe2.pdf) accompanying jacktrip demo at [Linux Audio Conference 2019](https://lac.linuxaudio.org/2019/)

# Other Repos
jacktrip (1.0) was released on google code. When that shut down, it migrated to github (1.05, 1.1)

The most current release (1.2) is here on CCRMA's cm-gitlab